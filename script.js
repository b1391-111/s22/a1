// SAMPLE
db.fruits.aggregate([
    {
        $match: {
            onSale : true
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            total: {
                $sum: "$stock"
            }
        }
    },
    {
        $project: {
            _id: 0
        }
    },
    {
        $sort: {
            total: 1
        }
    }
]);

// Solution 2
db.fruits.aggregate(
    [
        {
            $match: {
                onSale : true
            }
        },
        {
            $count: "fruitsOnSale"
        }
    ]
)

// Solution # 3
db.fruits.aggregate(
    [
        {
            $match: {
                stock: {
                    $gte: 20
                }
            }
        },
        {
            $count: "enoughStock"
        }
    ]
)

// Solution # 4
db.fruits.aggregate([
    {
        $match: {
            onSale : true
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            avg_price: {
                $avg: "$price"
            }
        }
    }
]);

// Solution # 5
db.fruits.aggregate([
    {
        $match: {
            onSale : true
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            max_price: {
                $max: "$price"
            }
        }
    }
]);

// Solution # 6
db.fruits.aggregate([
    {
        $match: {
            onSale : true
        }
    },
    {
        $group: {
            _id: "$supplier_id",
            min_price: {
                $min: "$price"
            }
        }
    }
]);